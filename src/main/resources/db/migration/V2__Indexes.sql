CREATE INDEX IF NOT EXISTS section_id_index ON section(id);

CREATE INDEX IF NOT EXISTS book_id_index ON book(id);

CREATE INDEX IF NOT EXISTS user_role_index ON users(role);