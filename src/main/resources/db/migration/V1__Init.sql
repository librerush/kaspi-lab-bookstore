CREATE TABLE IF NOT EXISTS section (
    id BIGSERIAL PRIMARY KEY,
    name varchar(256) not null
);

CREATE TABLE IF NOT EXISTS author (
    id BIGSERIAL PRIMARY KEY,
    first_name varchar(256) not null,
    last_name varchar(256) not null
);

CREATE TABLE IF NOT EXISTS book (
    id BIGSERIAL PRIMARY KEY,
    name varchar(256) not null,
    year int,
    description varchar(2048),
    price float not null,
    lang varchar(256) not null,
    isbn varchar(32),
    page int,
    image varchar(1024),
    quantity int not null,
    author_id BIGSERIAL references author(id),
    section_id BIGSERIAL references section(id)
);

CREATE TABLE IF NOT EXISTS users (
    id BIGSERIAL PRIMARY KEY,
    name varchar(256) not null,
    email varchar(256) not null,
    password varchar(256) not null,
    role varchar(256) not null
);

CREATE TABLE IF NOT EXISTS orders (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGSERIAL references users(id) not null,
    order_date timestamp without time zone default (now() at time zone 'utc') not null
);

CREATE TABLE IF NOT EXISTS orders_book (
    order_id BIGSERIAL references orders(id),
    book_id BIGSERIAL references book(id),
    quantity int not null,
    PRIMARY KEY (order_id, book_id),
    CONSTRAINT fk_order_id FOREIGN KEY (order_id) REFERENCES orders(id),
    CONSTRAINT fk_book_id FOREIGN KEY (book_id) REFERENCES book(id)
);
