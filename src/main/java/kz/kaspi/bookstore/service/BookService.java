package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.dto.BookDto;
import kz.kaspi.bookstore.model.Author;
import kz.kaspi.bookstore.model.Book;
import kz.kaspi.bookstore.model.Section;
import kz.kaspi.bookstore.repository.BookRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    private final BookRepository bookRepository;

    private final SectionService sectionService;

    private final AuthorService authorService;

    private final ImageService imageService;

    public BookService(BookRepository bookRepository, SectionService sectionService,
                       AuthorService authorService, ImageService imageService) {
        this.bookRepository = bookRepository;
        this.sectionService = sectionService;
        this.authorService = authorService;
        this.imageService = imageService;
    }

    @Transactional
    @Modifying
    public Book add(BookDto bookDto) {
        Book book = new Book();
        book.setName(bookDto.getName());
        book.setYear(bookDto.getYear());
        book.setDescription(bookDto.getDescription());
        book.setPrice(bookDto.getPrice());
        book.setLang(bookDto.getLang());
        book.setIsbn(bookDto.getIsbn());
        book.setPage(bookDto.getPage());
        if (bookDto.getImage() != null) {
            String image = imageService
                    .newFileName(bookDto.getImage().getOriginalFilename());
            if (imageService.save(bookDto.getImage(), image)) {
                book.setImage(image);
            }
        }
        book.setQuantity(bookDto.getQuantity());
        Optional<Section> section = sectionService.findByName(bookDto.getSection());
        section.ifPresent(book::setSection);
        Optional<Author> author = authorService.findByFullName(bookDto.getAuthor());
        author.ifPresent(book::setAuthor);
        return bookRepository.save(book);
    }

    @Transactional
    @Modifying
    public Book add(Book book) {
        return bookRepository.save(book);
    }

    @Transactional
    public List<Book> getAll() {
        return (List<Book>) bookRepository.findAll();
    }

    @Transactional
    public List<Book> findByName(String name) {
        return bookRepository.findBookByName(name);
    }

    @Transactional
    public Optional<Book> findById(Long id) {
        return bookRepository.findById(id);
    }

    @Transactional
    @Modifying
    public Book update(Book book) {
        return bookRepository.save(book);
    }

    public List<Book> findByPriceBetween(double start, double end) {
        return bookRepository.findDistinctByPriceBetween(start, end);
    }

    public Double findMaxPrice() {
        return bookRepository.findMaxPrice();
    }
}
