package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.dto.LoginDto;
import kz.kaspi.bookstore.dto.UserDto;
import kz.kaspi.bookstore.model.User;
import kz.kaspi.bookstore.repository.UserRepository;
import kz.kaspi.bookstore.util.PasswordEncoderUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserService {
    private final PasswordEncoderUtil passwdEncoder = new PasswordEncoderUtil();

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User registerNewUserAccount(UserDto userDto) throws IllegalArgumentException {
        if (emailExist(userDto.getEmail())) {
            throw new IllegalArgumentException("There is an account with that email address: "
                    + userDto.getEmail());
        }

        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setPassword(passwdEncoder.encode(userDto.getPassword()));
        user.setRole("USER");

        return userRepository.save(user);
    }

    public User registerNewAdmin(String password) throws IllegalArgumentException {
        final String email = "admin@bookstore.kz";
        final String name = "admin";

        if (emailExist(email)) {
            throw new IllegalArgumentException("There is admin account with that email address: " +
                    email);
        }

        User admin = new User();
        admin.setRole("ADMIN,USER");
        admin.setEmail(email);
        admin.setName(name);
        admin.setPassword(passwdEncoder.encode(password));
        return userRepository.save(admin);
    }

    public Optional<User> findAdmin() {
        return userRepository.findUserByRole("ADMIN,USER");
    }

    public boolean userExist(LoginDto loginDto) {
        Optional<User> user = findUserByEmail(loginDto.getEmail());
        if (user.isEmpty()) {
            return false;
        }
        return passwdEncoder
                .matches(loginDto.getPassword(), user.get().getPassword());
    }

    public Optional<User> findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    private boolean emailExist(String email) {
        return userRepository.findByEmail(email).isPresent();
    }
}
