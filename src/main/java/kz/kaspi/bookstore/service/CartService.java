package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.dto.CartDto;
import kz.kaspi.bookstore.model.Book;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Map;
import java.util.Optional;

@Service
public class CartService {
    private final BookService bookService;

    public CartService(BookService bookService) {
        this.bookService = bookService;
    }

    private boolean putBookInCart(Book book, CartDto cartDto, Model model) {
        Map<Book, Integer> books = cartDto.getBooks();
        if (book.getQuantity() < 1L) {
            model.addAttribute("message",
                    String.format("No more book left: %s", book.getName()));
            return false;
        }
        if (books.containsKey(book)) {
            int i = books.get(book);
            if (book.getQuantity() < i + 1) {
                model.addAttribute("message",
                        String.format("No more book left: %s", book.getName()));
                return false;
            } else {
                books.put(book, i + 1);
            }
        } else {
            books.put(book, 1);
        }
        return true;
    }

    public boolean addToCartIfExists(Long bookId, CartDto cartDto, Model model) {
        if (bookId != null && bookId > 0) {
            Optional<Book> bookOptional = bookService.findById(bookId);
            if (bookOptional.isPresent()) {
                Book book = bookOptional.get();
                return putBookInCart(book, cartDto, model);
            }
        }
        return false;
    }

    public boolean deleteFromCart(Long id, CartDto cartDto) {
        if (id != null && id >= 0) {
            int index = Math.toIntExact(id);
            cartDto.getBooks()
                    .entrySet()
                    .removeIf(bookIntegerEntry -> bookIntegerEntry.getKey().getId() == index);
            return true;
        }
        return false;
    }
}
