package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.model.Author;
import kz.kaspi.bookstore.repository.AuthorRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Transactional
    @Modifying
    public Author add(String firstName, String lastName) {
        return authorRepository.save(new Author(firstName, lastName));
    }

    @Transactional
    public Optional<Author> findByFirstAndLastName(String firstName, String lastName) {
        return authorRepository.findAuthorByFirstNameAndLastName(firstName, lastName);
    }

    public Optional<Author> findByFullName(String name) {
        String[] names = name.split(" ");
        if (names.length != 2) {
            return Optional.empty();
        } else {
            return findByFirstAndLastName(names[0], names[1]);
        }
    }

    public List<String> getAllFullNames() {
        return ((List<Author>) authorRepository.findAll())
                .stream()
                .map(author ->
                        String.format("%s %s", author.getFirstName(), author.getLastName()))
                .collect(Collectors.toList());
    }
}
