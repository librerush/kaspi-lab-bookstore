package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.dto.CartDto;
import kz.kaspi.bookstore.model.Book;
import kz.kaspi.bookstore.model.Order;
import kz.kaspi.bookstore.model.OrdersBook;
import kz.kaspi.bookstore.model.User;
import kz.kaspi.bookstore.repository.BookRepository;
import kz.kaspi.bookstore.repository.OrderRepository;
import kz.kaspi.bookstore.repository.OrdersBookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class OrderService {
    private final Logger logger = LoggerFactory.getLogger(OrderService.class);

    private final UserService userService;

    private final OrderRepository orderRepository;

    private final BookRepository bookRepository;

    private final OrdersBookRepository ordersBookRepository;

    public OrderService(UserService userService, OrderRepository orderRepository,
                        BookRepository bookRepository, OrdersBookRepository ordersBookRepository) {
        this.userService = userService;
        this.orderRepository = orderRepository;
        this.bookRepository = bookRepository;
        this.ordersBookRepository = ordersBookRepository;
    }

    @Transactional
    @Modifying
    public Order create(CartDto cart) {
        logger.warn(String.valueOf(cart));
        Optional<User> userOptional = userService.findUserByEmail(cart.getUserEmail());

        if (userOptional.isEmpty()) {
            throw new RuntimeException(String.format("No user with email: %s", cart.getUserEmail()));
        }
        Order order = new Order(userOptional.get(), LocalDateTime.now());
        order = orderRepository.save(order);

        Map<Book, Integer> books = cart.getBooks();
        for (Map.Entry<Book, Integer> bookEntry : books.entrySet()) {
            Book book = bookEntry.getKey();
            int quantity = bookEntry.getValue();
            if (book.getQuantity() < quantity) {
                throw new RuntimeException(String.format("Quantity must be %d for %s", quantity, book));
            }
            book.setQuantity(book.getQuantity() - quantity);
            book = bookRepository.save(book);
            OrdersBook ordersBook = new OrdersBook();
            ordersBook.setOrder(order);
            ordersBook.setBook(book);
            ordersBook.setQuantity(quantity);
            order.getOrdersBook().add(ordersBook);
            book.getOrdersBooks().add(ordersBook);
            ordersBookRepository.save(ordersBook);
        }
        return order;
    }

    public List<OrdersBook> getOrdersBookByUserEmail(String email) {
        return ordersBookRepository.findOrdersBooksByUserEmail(email);
    }
}
