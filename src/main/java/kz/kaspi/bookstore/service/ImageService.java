package kz.kaspi.bookstore.service;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

@Service
public class ImageService {
    private final static String STORAGE_PATH = "static/image/";

    private Clock clock;

    /**
     * Get an unique file name.
     */
    public String newFileName(String path) {
        if (clock == null) {
            clock = Clock.systemDefaultZone();
        }
        LocalDateTime localDateTime = LocalDateTime.now(clock);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        return String.format("%d-%s", zonedDateTime.toInstant().toEpochMilli(), path);
    }

    /**
     * Save an uploaded file.
     * @return true if file is saved
     */
    public boolean save(MultipartFile multipartFile, String name) {
        if (multipartFile == null || multipartFile.isEmpty() ||
                multipartFile.getSize() > 10000000L) { /* more than 10 MB */
            return false;
        }
        String path = String.format("%s%s", STORAGE_PATH, name);
        try {
            multipartFile.transferTo(new File(path).getAbsoluteFile());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Get saved image as byte array.
     */
    public Optional<byte []> get(String path) {
        byte[] bytes;
        String dataPath = String.format("%s%s", STORAGE_PATH, path);
        try {
            bytes = FileUtils
                    .readFileToByteArray(new File(dataPath));
        } catch (IOException e) {
            e.printStackTrace();
            return Optional.empty();
        }
        if (bytes == null || bytes.length < 1) {
            return Optional.empty();
        }
        return Optional.of(bytes);
    }
}
