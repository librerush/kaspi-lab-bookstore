package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.model.Section;
import kz.kaspi.bookstore.repository.SectionRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SectionService {
    private final SectionRepository sectionRepository;

    public SectionService(SectionRepository sectionRepository) {
        this.sectionRepository = sectionRepository;
    }

    @Transactional
    @Modifying
    public Section add(String name) {
        return sectionRepository.save(new Section(name));
    }

    @Transactional
    public List<Section> getAll() {
        return (List<Section>) sectionRepository.findAll();
    }

    @Transactional
    public Optional<Section> findByName(String name) {
      return sectionRepository.findByName(name);
    }
}
