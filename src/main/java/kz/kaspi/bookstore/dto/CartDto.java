package kz.kaspi.bookstore.dto;

import kz.kaspi.bookstore.model.Book;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CartDto {
    private String userEmail;

    private Map<Book, Integer> books;

    public CartDto() {
        books = new HashMap<>();
    }

    public CartDto(String userId, Map<Book, Integer> books) {
        this.userEmail = userId;
        this.books = books;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Map<Book, Integer> getBooks() {
        return books;
    }

    public void setBooks(Map<Book, Integer> books) {
        this.books = books;
    }

    public long totalPrice() {
        long total = 0L;
        if (books == null || books.isEmpty()) {
            return total;
        }
        for (Map.Entry<Book, Integer> book : books.entrySet()) {
            total += book.getKey().getPrice() * book.getValue();
        }
        return total;
    }

    @Override
    public String toString() {
        return "CartDto{" +
                "userEmail=" + userEmail +
                ", books=" + books +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartDto cartDto = (CartDto) o;
        return Objects.equals(userEmail, cartDto.userEmail) && Objects.equals(books, cartDto.books);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userEmail, books);
    }
}
