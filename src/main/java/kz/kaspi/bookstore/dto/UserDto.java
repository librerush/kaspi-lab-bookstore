package kz.kaspi.bookstore.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

public class UserDto {
    @NotNull(message = "Name must be non-null")
    @NotEmpty(message = "Name must be non-empty")
    @Size(min = 2, max = 30, message = "Name must contain at least 2 characters")
    private String name;

    @NotNull(message = "Email must be non-null")
    @NotEmpty(message = "Email must be non-empty")
    @Size(min = 4, max = 200, message = "Email must contain at least 4 characters")
    @Email
    private String email;

    @NotNull
    @NotEmpty
    @Size(min = 4, max = 40, message = "Password must contain at least 4 characters")
    private String password;
    @NotNull
    @NotEmpty
    @Size(min = 4, max = 40, message = "Matching password must contain at least 4 characters")
    private String matchingPassword;

    public UserDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(name, userDto.name) && Objects.equals(email, userDto.email) && Objects.equals(password, userDto.password) && Objects.equals(matchingPassword, userDto.matchingPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, password, matchingPassword);
    }
}
