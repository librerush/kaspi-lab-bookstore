package kz.kaspi.bookstore.dto;

import org.springframework.web.multipart.MultipartFile;

public class BookDto {
    private String name;

    private long year;

    private String description;

    private double price;

    private String lang;

    private String isbn;

    private long page;

    private MultipartFile image;

    private long quantity;

    private String section;

    private String author;

    public BookDto() {
    }

    public BookDto(String name, long year, String description, double price, String lang,
                   String isbn, long page, MultipartFile image, long quantity,
                   String section, String author) {
        this.name = name;
        this.year = year;
        this.description = description;
        this.price = price;
        this.lang = lang;
        this.isbn = isbn;
        this.page = page;
        this.image = image;
        this.quantity = quantity;
        this.section = section;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "BookDto{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", lang='" + lang + '\'' +
                ", isbn='" + isbn + '\'' +
                ", page=" + page +
                ", image='" + image + '\'' +
                ", quantity=" + quantity +
                ", section='" + section + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
