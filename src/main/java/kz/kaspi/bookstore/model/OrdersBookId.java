package kz.kaspi.bookstore.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class OrdersBookId implements Serializable {
    private static final long serialVersionUID = 1L;

    private long bookId;

    private long orderId;

    public OrdersBookId() {
    }

    public OrdersBookId(long bookId, long orderId) {
        super();
        this.bookId = bookId;
        this.orderId = orderId;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdersBookId that = (OrdersBookId) o;
        return bookId == that.bookId && orderId == that.orderId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId, orderId);
    }
}
