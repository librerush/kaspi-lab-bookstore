package kz.kaspi.bookstore.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "orders_book")
public class OrdersBook {
    @EmbeddedId
    private OrdersBookId id = new OrdersBookId();

    @ManyToOne
    @MapsId("orderId")
    private Order order;

    @ManyToOne
    @MapsId("bookId")
    private Book book;

    private long quantity;

    public OrdersBook() {
    }

    public OrdersBook(Order order, Book book, long quantity) {
        this.order = order;
        this.book = book;
        this.quantity = quantity;
    }

    public OrdersBookId getId() {
        return id;
    }

    public void setId(OrdersBookId id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "OrdersBook{" +
                "id=" + id +
                ", order=" + order +
                ", book=" + book +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdersBook that = (OrdersBook) o;
        return quantity == that.quantity && Objects.equals(id, that.id) && Objects.equals(order, that.order) && Objects.equals(book, that.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, order, book, quantity);
    }
}
