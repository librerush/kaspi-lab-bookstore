package kz.kaspi.bookstore.model;

import kz.kaspi.bookstore.dto.BookDto;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private long year;

    @Column(length = 2048)
    private String description;

    private double price;

    private String lang;

    private String isbn;

    private long page;

    @Column(length = 1024)
    private String image;

    @ManyToOne
    @JoinColumn(name = "section_id")
    private Section section;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    private long quantity;

    @OneToMany(mappedBy = "book")
    private Set<OrdersBook> ordersBooks = new HashSet<>();

    public Book() {
    }

    public Book(String name, long year, String description, double price,
                String lang, String isbn, long page, String image, Section section,
                Author author, long quantity) {
        this.name = name;
        this.year = year;
        this.description = description;
        this.price = price;
        this.lang = lang;
        this.isbn = isbn;
        this.page = page;
        this.image = image;
        this.section = section;
        this.author = author;
        this.quantity = quantity;
    }

    public Book(BookDto bookDto) {
        this.setName(bookDto.getName());
        this.setYear(bookDto.getYear());
        this.setDescription(bookDto.getDescription());
        this.setPrice(bookDto.getPrice());
        this.setLang(bookDto.getLang());
        this.setIsbn(bookDto.getIsbn());
        this.setPage(bookDto.getPage());
        this.setImage(bookDto.getImage().getOriginalFilename());
        this.setQuantity(bookDto.getQuantity());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Set<OrdersBook> getOrdersBooks() {
        return ordersBooks;
    }

    public void setOrdersBooks(Set<OrdersBook> ordersBooks) {
        this.ordersBooks = ordersBooks;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", lang='" + lang + '\'' +
                ", isbn='" + isbn + '\'' +
                ", page=" + page +
                ", image='" + image + '\'' +
                ", section=" + section +
                ", author=" + author +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id && year == book.year && Double.compare(book.price, price) == 0 && page == book.page && quantity == book.quantity && Objects.equals(name, book.name) && Objects.equals(description, book.description) && Objects.equals(lang, book.lang) && Objects.equals(isbn, book.isbn) && Objects.equals(image, book.image) && Objects.equals(section, book.section) && Objects.equals(author, book.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, year, description, price, lang, isbn, page, image, section, author, quantity);
    }
}
