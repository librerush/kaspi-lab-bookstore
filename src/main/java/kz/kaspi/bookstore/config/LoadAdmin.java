package kz.kaspi.bookstore.config;

import kz.kaspi.bookstore.model.User;
import kz.kaspi.bookstore.service.UserService;
import kz.kaspi.bookstore.util.PasswordEncoderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Configuration
public class LoadAdmin {
    @Value("${ADMIN_PASSWORD}")
    private String adminPassword;

    private final Logger logger = LoggerFactory.getLogger(LoadAdmin.class);

    private final PasswordEncoderUtil passwdEncoder = new PasswordEncoderUtil();

    private final UserService userService;

    public LoadAdmin(UserService userService) {
        this.userService = userService;
    }

    @Bean
    @Transactional
    @Modifying
    CommandLineRunner initAdmin() {
        return args -> {
            if (adminPassword == null || adminPassword.isEmpty()) {
                throw new IllegalArgumentException("Empty ${ADMIN_PASSWORD}");
            }
            Optional<User> adminOpt = userService.findAdmin();
            if (adminOpt.isEmpty()) {
                User admin = userService.registerNewAdmin(adminPassword);
                logger.info(String.format("Created new admin: %s", admin));
            } else {
                if (!passwdEncoder.matches(adminPassword, adminOpt.get().getPassword())) {
                    throw new IllegalArgumentException(
                            "Admin's password doesnt match with ${ADMIN_PASSWORD}");
                }
            }
        };
    }
}
