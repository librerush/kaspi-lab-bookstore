package kz.kaspi.bookstore.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordEncoderUtil {
    private final PasswordEncoder passwordEncoder;

    public PasswordEncoderUtil() {
        this.passwordEncoder = new BCryptPasswordEncoder();
    }

    public String encode(final String password) {
        return passwordEncoder.encode(password);
    }

    public boolean matches(final String raw, final String encoded) {
        return passwordEncoder.matches(raw, encoded);
    }
}
