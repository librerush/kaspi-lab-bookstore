package kz.kaspi.bookstore.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;

import java.util.Optional;

public class Util {
    public static Optional<UserDetails> getUserDetailsIfAuthenticated(Authentication authentication, Model model) {
        UserDetails userDetails;
        if (authentication != null && authentication.isAuthenticated()) {
            userDetails = (UserDetails) authentication.getPrincipal();
            model.addAttribute("user", userDetails);
            return Optional.of(userDetails);
        }
        return Optional.empty();
    }
}
