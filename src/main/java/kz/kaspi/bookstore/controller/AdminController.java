package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.dto.BookDto;
import kz.kaspi.bookstore.model.Book;
import kz.kaspi.bookstore.model.Section;
import kz.kaspi.bookstore.service.AuthorService;
import kz.kaspi.bookstore.service.BookService;
import kz.kaspi.bookstore.service.SectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final Logger logger = LoggerFactory.getLogger(AdminController.class);

    private final SectionService sectionService;

    private final AuthorService authorService;

    private final BookService bookService;

    public AdminController(SectionService sectionService, AuthorService authorService, BookService bookService) {
        this.sectionService = sectionService;
        this.authorService = authorService;
        this.bookService = bookService;
    }

    @GetMapping
    public String show(Authentication authentication, Model model) {
        addAdminToModel(authentication, model);
        return "admin";
    }

    @GetMapping("/section")
    public String showSection(Authentication authentication, Model model) {
        addAdminToModel(authentication, model);
        return "admin/section";
    }

    @PostMapping("/section")
    public String addSection(Authentication authentication, Model model,
                             @RequestParam(name = "section") String name) {
        addAdminToModel(authentication, model);
        sectionService.add(name);
        return "admin";
    }

    @GetMapping("/section/list")
    public String showSectionList(Authentication authentication, Model model) {
        addAdminToModel(authentication, model);
        List<Section> sectionList = sectionService.getAll();
        model.addAttribute("sections", sectionList);
        return "admin/section-list";
    }

    @GetMapping("/author")
    public String showAuthor(Authentication authentication, Model model) {
        addAdminToModel(authentication, model);
        return "admin/author";
    }

    @PostMapping("/author")
    public String addAuthor(Authentication authentication, Model model,
                            @RequestParam(name = "first_name") String firstName,
                            @RequestParam(name = "last_name") String lastName) {
        addAdminToModel(authentication, model);
        authorService.add(firstName, lastName);
        return "admin";
    }

    @GetMapping("/book")
    public String showBook(Authentication authentication, Model model,
                           @RequestParam(required = false, name = "id") Long id) {
        addAdminToModel(authentication, model);
        model.addAttribute("sections", sectionService.getAll());
        model.addAttribute("authors", authorService.getAllFullNames());
        if (id != null && id > 0) {
            Optional<Book> book = bookService.findById(id);
            book.ifPresent(value -> model.addAttribute("book", value));
        }
        return "admin/book";
    }

    @PostMapping(value = "/book")
    public String addBook(Authentication authentication, Model model,
                          @ModelAttribute BookDto bookDto) {
        addAdminToModel(authentication, model);
        logger.warn(bookDto.toString());
        bookService.add(bookDto);
        return "admin";
    }

    @PostMapping("/book/update")
    public String updateBook(Authentication authentication, Model model,
                             @RequestParam("id") Long id,
                             @RequestParam("name") String name,
                             @RequestParam("description") String desc,
                             @RequestParam("quantity") Long quantity,
                             @RequestParam("price") Double price) {
        addAdminToModel(authentication, model);
        Optional<Book> bookOptional = bookService.findById(id);
        if (bookOptional.isPresent()) {
            Book book = bookOptional.get();
            book.setName(name);
            book.setDescription(desc);
            book.setQuantity(quantity);
            book.setPrice(price);
            bookService.update(book);
        }
        return "admin";
    }

    private void addAdminToModel(Authentication authentication, Model model) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        model.addAttribute("admin", userDetails);
    }
}
