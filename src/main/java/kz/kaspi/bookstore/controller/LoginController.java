package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.dto.CartDto;
import kz.kaspi.bookstore.dto.LoginDto;
import kz.kaspi.bookstore.model.Book;
import kz.kaspi.bookstore.model.User;
import kz.kaspi.bookstore.service.BookService;
import kz.kaspi.bookstore.service.UserDetailsServiceImpl;
import kz.kaspi.bookstore.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static kz.kaspi.bookstore.util.Util.getUserDetailsIfAuthenticated;

@Controller
public class LoginController {
    private final BookService bookService;

    private final UserDetailsServiceImpl userDetailsService;

    private final UserService userService;

    public LoginController(BookService bookService, UserDetailsServiceImpl userDetailsService,
                           UserService userService) {
        this.bookService = bookService;
        this.userDetailsService = userDetailsService;
        this.userService = userService;
    }

    @GetMapping("/")
    public String index(Authentication authentication, Model model,
                        @RequestParam(required = false, name = "section") Long sectionId,
                        @ModelAttribute("cart") CartDto cartDto) {
        getUserDetailsIfAuthenticated(authentication, model);
        List<Book> bookList;
        if (sectionId != null && sectionId > 0) {
            bookList = bookService.getAll()
                    .stream()
                    .filter(book -> book.getSection().getId() == sectionId)
                    .collect(Collectors.toList());
        } else {
            bookList = bookService.getAll();
        }
        model.addAttribute("books", bookList);
        return "index";
    }

    @GetMapping("/user/login")
    public String showLoginForm(Model model, @RequestParam(required = false) Boolean error) {
        model.addAttribute("loginDto", new LoginDto());
        if (error != null && error) {
            model.addAttribute("error", "Wrong email or password");
        }
        return "login";
    }

    @PostMapping("/user/login")
    public String login(Model model,
                        @ModelAttribute("loginDto") LoginDto loginDto) {
        if (userService.userExist(loginDto)) {
            Optional<User> userOptional = userService.findUserByEmail(loginDto.getEmail());
            UserDetails userDetails = userDetailsService.loadUserByUsername(loginDto.getEmail());
            userOptional.ifPresent(user -> model.addAttribute("user", userDetails));
            return "index";
        } else {
            model.addAttribute("error", "No such user");
            return "login";
        }
    }
}
