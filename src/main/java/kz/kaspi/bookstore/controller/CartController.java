package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.dto.CartDto;
import kz.kaspi.bookstore.service.CartService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static kz.kaspi.bookstore.util.Util.getUserDetailsIfAuthenticated;

@Controller
@RequestMapping("/cart")
@SessionAttributes("cart")
public class CartController {
    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @ModelAttribute
    public void addAttr(Model model) {
        CartDto cart = (CartDto) model.getAttribute("cart");
        if (cart == null) {
            cart = new CartDto();
        }
        model.addAttribute("cart", cart);
    }

    @GetMapping
    public String index(Authentication authentication, Model model,
                        @ModelAttribute("cart") CartDto cartDto,
                        @RequestParam(required = false, name = "delete") Long id) {
        Optional<UserDetails> userDetails = getUserDetailsIfAuthenticated(authentication, model);
        userDetails.ifPresent(details -> cartDto.setUserEmail(details.getUsername()));
        cartService.deleteFromCart(id, cartDto);
        return "cart";
    }

    @PostMapping
    public String add(Authentication authentication, Model model,
                      @RequestParam("id") Long bookId,
                      @ModelAttribute("cart") CartDto cartDto) {
        Optional<UserDetails> userDetails = getUserDetailsIfAuthenticated(authentication, model);
        userDetails.ifPresent(details -> cartDto.setUserEmail(details.getUsername()));
        cartService.addToCartIfExists(bookId, cartDto, model);
        return "cart";
    }
}
