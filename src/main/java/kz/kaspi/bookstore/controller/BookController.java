package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.model.Book;
import kz.kaspi.bookstore.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static kz.kaspi.bookstore.util.Util.getUserDetailsIfAuthenticated;

@Controller
@RequestMapping("/book")
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/{id}")
    public String showBook(Authentication authentication,
                           Model model,
                           @PathVariable Long id) {
        Optional<Book> bookOpt = bookService.findById(id);
        if (bookOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("No such book: %d", id));
        }
        getUserDetailsIfAuthenticated(authentication, model);
        model.addAttribute("book", bookOpt.get());
        return "book";
    }

    @GetMapping
    public String findBooks(Authentication authentication,
                            Model model,
                            @RequestParam("q") String query) {
        List<Book> bookList = bookService.findByName(query);
        getUserDetailsIfAuthenticated(authentication, model);
        model.addAttribute("books", bookList);
        return "index";
    }

    @GetMapping("/filter")
    public String filterBooks(Authentication authentication,
                              Model model) {
        getUserDetailsIfAuthenticated(authentication, model);
        model.addAttribute("max", bookService.findMaxPrice());
        return "filter";
    }

    @PostMapping("/filter")
    public String handleFilter(Authentication authentication,
                               Model model,
                               @RequestParam("range") double range) {
        getUserDetailsIfAuthenticated(authentication, model);
        List<Book> bookList = bookService.findByPriceBetween(1.0, range);
        model.addAttribute("books", bookList);
        return "index";
    }
}
