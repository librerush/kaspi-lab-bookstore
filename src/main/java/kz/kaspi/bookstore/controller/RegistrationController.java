package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.dto.UserDto;
import kz.kaspi.bookstore.model.User;
import kz.kaspi.bookstore.service.UserDetailsServiceImpl;
import kz.kaspi.bookstore.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class RegistrationController {
    private final UserService userService;

    private final UserDetailsServiceImpl userDetailsService;

    public RegistrationController(UserService userService, UserDetailsServiceImpl userDetailsService) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/user/registration")
    public String showRegistrationForm(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
        return "registration";
    }

    @PostMapping("/user/registration")
    public ModelAndView registerUserAccount(@Valid UserDto userDto, BindingResult result) {
        ModelAndView mav = new ModelAndView("registration");
        if (result.hasErrors()) {
            String err = fieldErrorsToString(result.getFieldErrors());
            mav.addObject("message", err);
            return mav;
        }
        if (!userDto.getPassword().equals(userDto.getMatchingPassword())) {
            mav.addObject("message", "Passwords must be same");
            return mav;
        }
        User user;
        try {
            user = userService.registerNewUserAccount(userDto);
        } catch (IllegalArgumentException e) {
            mav.addObject("message", "An account for that email already exists.");
            return mav;
        }
        UserDetails userDetails = userDetailsService.UserToUserDetails(user);
        return new ModelAndView("index", "user", userDetails);
    }

    private String fieldErrorsToString(List<FieldError> fieldErrorList) {
        return fieldErrorList
                .stream()
                .map(fieldError ->
                        String.format("%s: must contain %d characters.\n",
                                fieldError.getField(),
                                "name".equals(fieldError.getField()) ? 2 : 4))
                .collect(Collectors.joining());
    }
}
