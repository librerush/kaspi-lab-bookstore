package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.dto.CartDto;
import kz.kaspi.bookstore.model.OrdersBook;
import kz.kaspi.bookstore.service.OrderService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import java.util.List;
import java.util.Optional;

import static kz.kaspi.bookstore.util.Util.getUserDetailsIfAuthenticated;

@Controller
@RequestMapping("/order")
@SessionAttributes("cart")
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    public String addOrder(Authentication authentication, Model model,
                           @ModelAttribute("cart") CartDto cartDto,
                           SessionStatus sessionStatus) {
        Optional<UserDetails> userDetails = getUserDetailsIfAuthenticated(authentication, model);
        orderService.create(cartDto);
        sessionStatus.setComplete();
        if (userDetails.isPresent()) {
            List<OrdersBook> ordersBookList = orderService
                    .getOrdersBookByUserEmail(userDetails.get().getUsername());
            model.addAttribute("orders", ordersBookList);
        }
        return "orders";
    }

    @GetMapping
    public String index(Authentication authentication, Model model) {
        Optional<UserDetails> userDetails = getUserDetailsIfAuthenticated(authentication, model);
        if (userDetails.isPresent()) {
            List<OrdersBook> ordersBookList = orderService
                    .getOrdersBookByUserEmail(userDetails.get().getUsername());
            model.addAttribute("orders", ordersBookList);
        }
        return "orders";
    }
}
