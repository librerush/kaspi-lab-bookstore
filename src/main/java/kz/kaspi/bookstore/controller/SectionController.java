package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.model.Section;
import kz.kaspi.bookstore.service.SectionService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static kz.kaspi.bookstore.util.Util.getUserDetailsIfAuthenticated;

@Controller
@RequestMapping("/section")
public class SectionController {
    private final SectionService sectionService;

    public SectionController(SectionService sectionService) {
        this.sectionService = sectionService;
    }

    @GetMapping
    public String index(Authentication authentication, Model model) {
        getUserDetailsIfAuthenticated(authentication, model);
        List<Section> sectionList = sectionService.getAll();
        model.addAttribute("sections", sectionList);
        return "section";
    }
}
