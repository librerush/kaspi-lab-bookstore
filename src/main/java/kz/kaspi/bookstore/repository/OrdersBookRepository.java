package kz.kaspi.bookstore.repository;

import kz.kaspi.bookstore.model.Order;
import kz.kaspi.bookstore.model.OrdersBook;
import kz.kaspi.bookstore.model.OrdersBookId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersBookRepository extends CrudRepository<OrdersBook, OrdersBookId> {
    List<OrdersBook> findOrdersBooksByOrder(Order order);

    @Query("select ob from OrdersBook ob\n" +
            "    join Order o on ob.order = o join User u on o.user = u.id\n" +
            "    where u.email = :email")
    List<OrdersBook> findOrdersBooksByUserEmail(String email);
}
