package kz.kaspi.bookstore.repository;

import kz.kaspi.bookstore.model.Order;
import kz.kaspi.bookstore.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
    List<Order> findOrderByUser(User user);
}
