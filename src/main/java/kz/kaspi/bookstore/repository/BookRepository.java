package kz.kaspi.bookstore.repository;

import kz.kaspi.bookstore.model.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
    List<Book> findBookByNameLikeIgnoreCase(String name);

    @Query(value = "SELECT b FROM Book b WHERE fts_book(:description, :description) = true")
    List<Book> findBookByName(@Param("description") String description);

    List<Book> findDistinctByPriceBetween(double start, double end);

    @Query("select max(b.price) from Book b")
    Double findMaxPrice();
}
