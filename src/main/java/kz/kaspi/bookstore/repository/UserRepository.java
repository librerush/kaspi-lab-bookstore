package kz.kaspi.bookstore.repository;

import kz.kaspi.bookstore.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByEmail(String email);
    Optional<User> findUserByRole(String role);
    Optional<User> findUserByEmail(String email);
}
