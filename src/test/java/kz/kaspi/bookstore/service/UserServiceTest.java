package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.dto.UserDto;
import kz.kaspi.bookstore.model.User;
import kz.kaspi.bookstore.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    private UserDetailsServiceImpl userDetailsService;

    @InjectMocks
    private UserService userService;

    private UserDto userDto;

    private final User user = new User("user", "user@mail.com", "pass", "USER");

    @BeforeEach
    public void setUp() {
        userDto = new UserDto();
        userDto.setEmail("user@mail.com");
        userDto.setName("user");
        userDto.setPassword("pass");
        userDto.setMatchingPassword("pass");
    }

    @Test
    public void shouldCreateNewUser() {
        when(userRepository.save(any())).then(returnsFirstArg());

        assertThat(userService.registerNewUserAccount(userDto))
                .hasToString(user.toString());
    }

    @Test
    public void shouldCreateNewAdmin() {
        User admin = new User("admin", "admin@bookstore.kz", "pass", "ADMIN,USER");

        assertThat(userService.registerNewAdmin("pass"))
                .hasToString(admin.toString());
    }

    @Test
    public void shouldLoadUser() {
        userDetailsService = new UserDetailsServiceImpl(userRepository);

        when(userRepository.findByEmail("user@mail.com"))
                .thenReturn(Optional.of(user));

        assertThat(userDetailsService.loadUserByUsername("user@mail.com"))
                .isInstanceOf(UserDetails.class)
                .isNotNull();
    }
}
