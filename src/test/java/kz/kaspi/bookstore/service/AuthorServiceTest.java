package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.model.Author;
import kz.kaspi.bookstore.repository.AuthorRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthorServiceTest {
    @Mock
    private AuthorRepository authorRepository;

    @InjectMocks
    private AuthorService authorService;

    @Test
    public void shouldFindAuthorByFullName() {
        Author author = new Author("Jay", "Sussman");
        when(authorRepository.findAuthorByFirstNameAndLastName("Jay", "Sussman"))
                .thenReturn(Optional.of(author));
        assertThat(authorService.findByFullName("Jay Sussman"))
                .isEqualTo(Optional.of(author));
    }

    @Test
    public void shouldFindAllFullNames() {
        Author author = new Author("Marcus", "Aurelius");
        when(authorRepository.findAll())
                .thenReturn(Collections.singletonList(author));
        assertThat(authorService.getAllFullNames())
                .isEqualTo(Collections.singletonList("Marcus Aurelius"));
    }
}
