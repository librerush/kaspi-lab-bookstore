package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.model.Section;
import kz.kaspi.bookstore.repository.SectionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SectionServiceTest {
    @Mock
    private SectionRepository sectionRepository;

    @InjectMocks
    private SectionService sectionService;

    @Test
    public void shouldFindSectionByName() {
        Section section = new Section("Foo");
        when(sectionRepository.findByName("Foo"))
                .thenReturn(Optional.of(section));
        assertThat(sectionService.findByName("Foo"))
                .isEqualTo(Optional.of(section));
    }
}
