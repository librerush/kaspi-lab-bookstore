package kz.kaspi.bookstore.service;

import kz.kaspi.bookstore.dto.BookDto;
import kz.kaspi.bookstore.model.Author;
import kz.kaspi.bookstore.model.Book;
import kz.kaspi.bookstore.model.Section;
import kz.kaspi.bookstore.repository.AuthorRepository;
import kz.kaspi.bookstore.repository.BookRepository;
import kz.kaspi.bookstore.repository.SectionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BookServiceTest {
    @Mock
    private SectionRepository sectionRepository;

    @InjectMocks
    private SectionService sectionService;

    @Mock
    private AuthorRepository authorRepository;

    @InjectMocks
    private AuthorService authorService;

    @InjectMocks
    private ImageService imageService;

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookService bookService;

    @Test
    public void shouldAddAndFind() {
        bookService = new BookService(bookRepository, sectionService,
                authorService, imageService);

        String name = "Meditations";
        long year = 160;
        String description = "A series of personal writings by Marcus Aurelius";
        double price = 2000;
        String lang = "EN";
        String isbn = "ISBN";
        long page = 200;
        long quantity = 10;
        String sectionStr = "Philosophy";
        String authorStr = "Marcus Aurelius";

        BookDto bookDto = new BookDto(name, year,
                description, price,
                lang, isbn, page, null, quantity,
                sectionStr, authorStr);

        Section section = new Section("Philosophy");
        Author author = new Author("Marcus", "Aurelius");

        Book book = new Book(name, year,
                description, price, lang,
                isbn, page, null,
                section, author, quantity);

        when(bookRepository.save(any()))
                .then(returnsFirstArg());

        when(sectionService.findByName("Philosophy"))
                .thenReturn(Optional.of(section));
        when(authorService.findByFirstAndLastName("Marcus", "Aurelius"))
                .thenReturn(Optional.of(author));

        assertThat(bookService.add(bookDto)).isEqualTo(book);
    }

    @Test
    public void shouldFindBooksBetweenTwoPrices() {
        Book book1 = mock(Book.class);
        Book book2 = mock(Book.class);
        book1.setPrice(200);
        book2.setPrice(300);
        List<Book> bookList = Arrays.asList(book1, book2);

        when(bookRepository.findDistinctByPriceBetween(200, 300))
                .thenReturn(bookList);

        assertThat(bookService.findByPriceBetween(200, 300))
                .isEqualTo(bookList);
    }
}
