package kz.kaspi.bookstore.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ImageServiceTest {
    @InjectMocks
    private ImageService imageService;

    @Mock
    private Clock clock;

    @Test
    public void shouldReturnUniqueFileName() {
        Clock fixedClock = Clock.fixed(Instant.now(), ZoneId.systemDefault());
        when(clock.instant()).thenReturn(fixedClock.instant());
        when(clock.getZone()).thenReturn(fixedClock.getZone());
        assertThat(imageService.newFileName("foo.jpg"))
                .isEqualTo(String.format("%d-foo.jpg", fixedClock.instant().toEpochMilli()));
    }
}
