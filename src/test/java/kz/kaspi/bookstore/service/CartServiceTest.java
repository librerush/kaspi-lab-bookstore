package kz.kaspi.bookstore.service;


import kz.kaspi.bookstore.dto.CartDto;
import kz.kaspi.bookstore.model.Book;
import kz.kaspi.bookstore.repository.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CartServiceTest {
    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookService bookService;

    private CartService cartService;

    @Mock
    private Model model;

    @Mock
    private CartDto cartDto;


    @BeforeEach
    public void setUp() {
        cartService = new CartService(bookService);
    }

    @Test
    public void shouldAddToCart() {
        Book book = new Book();
        book.setId(1L);
        book.setQuantity(1L);
        when(bookService.findById(anyLong())).thenReturn(Optional.of(book));

        assertThat(cartService.addToCartIfExists(1L, cartDto, model)).isTrue();
        assertThat(model.containsAttribute("message")).isFalse();
    }

    @Test
    public void shouldDeleteFromCart() {
        Book book = new Book();
        book.setId(1L);
        book.setQuantity(1L);
        cartDto.getBooks().put(book, 1);

        assertThat(cartService.deleteFromCart(1L, cartDto)).isTrue();
        assertThat(cartDto.getBooks().containsKey(book)).isFalse();
    }
}
