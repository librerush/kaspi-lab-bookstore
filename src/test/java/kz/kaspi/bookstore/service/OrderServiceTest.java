package kz.kaspi.bookstore.service;


import kz.kaspi.bookstore.dto.CartDto;
import kz.kaspi.bookstore.model.*;
import kz.kaspi.bookstore.repository.BookRepository;
import kz.kaspi.bookstore.repository.OrderRepository;
import kz.kaspi.bookstore.repository.OrdersBookRepository;
import kz.kaspi.bookstore.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OrderServiceTest {
    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private OrdersBookRepository ordersBookRepository;

    private OrderService orderService;

    private Book book;

    private CartDto cartDto;

    private User user;

    @BeforeEach
    public void setUp() {
        orderService = new OrderService(userService, orderRepository, bookRepository, ordersBookRepository);

        user = new User("User", "user@mail.com", "pass", "USER");
        book = newBook();
        cartDto = new CartDto(user.getEmail(), Collections.singletonMap(book, 1));
    }

    @Test
    public void shouldCreateOrdersBook() {
        when(userService.findUserByEmail("user@mail.com")).thenReturn(Optional.of(user));
        when(orderRepository.save(any())).then(returnsFirstArg());
        when(bookRepository.save(any())).then(returnsFirstArg());
        when(ordersBookRepository.save(any())).then(returnsFirstArg());

        Order order = orderService.create(cartDto);
        assertThat(order).isNotNull();
        assertThat(order.getUser()).isEqualTo(user);
        assertThat(order.getOrdersBook().isEmpty()).isFalse();
    }

    @Test
    public void shouldFindOrdersBookByEmail() {
        OrdersBook ordersBook = new OrdersBook();
        when(ordersBookRepository.findOrdersBooksByUserEmail("user@mail.com"))
                .thenReturn(Collections.singletonList(ordersBook));
        assertThat(orderService.getOrdersBookByUserEmail("user@mail.com"))
                .isEqualTo(Collections.singletonList(ordersBook));
    }

    private Book newBook() {
        return new Book("Name", 2021, "Description", 200.0, "EN",
                "ISBN", 200L, "image.png", new Section("section"),
                new Author("Foo", "Bar"), 10);
    }
}
