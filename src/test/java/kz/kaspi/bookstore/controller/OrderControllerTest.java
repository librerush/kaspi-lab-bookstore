package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.dto.CartDto;
import kz.kaspi.bookstore.model.Order;
import kz.kaspi.bookstore.model.OrdersBook;
import kz.kaspi.bookstore.service.OrderService;
import kz.kaspi.bookstore.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrderController.class)
public class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private OrderService orderService;

    @Mock
    private CartDto cartDto;

    @Mock
    private Order order;

    @WithMockUser
    @Test
    public void shouldCreate() throws Exception {
        List<OrdersBook> ordersBookList = new ArrayList<>();
        when(orderService.create(cartDto)).thenReturn(order);
        when(orderService.getOrdersBookByUserEmail(any()))
                .thenReturn(ordersBookList);

        mockMvc.perform(post("/order")
                .sessionAttr("cart", cartDto))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model()
                        .attribute("orders", ordersBookList));
    }
}
