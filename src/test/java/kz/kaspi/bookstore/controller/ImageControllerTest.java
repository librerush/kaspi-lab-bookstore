package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.service.ImageService;
import kz.kaspi.bookstore.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ImageController.class)
public class ImageControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private ImageService imageService;

    @Test
    public void shouldReturnNotFoundStatus() throws Exception {
        when(imageService.get(any())).thenReturn(Optional.empty());

        mockMvc.perform(get("/image/{path}","random-picture.jpg")
                .contentType(MediaType.IMAGE_JPEG))
                .andExpect(status().isNotFound());
    }
}
