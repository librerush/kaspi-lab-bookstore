package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.dto.CartDto;
import kz.kaspi.bookstore.model.Author;
import kz.kaspi.bookstore.model.Book;
import kz.kaspi.bookstore.model.Section;
import kz.kaspi.bookstore.service.BookService;
import kz.kaspi.bookstore.service.CartService;
import kz.kaspi.bookstore.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(CartController.class)
public class CartControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private BookService bookService;

    @MockBean
    private CartService cartService;

    private CartDto cartDto;

    private Book book;

    @BeforeEach
    public void setUp() {
        book = new Book();
        cartDto = new CartDto();

        book.setAuthor(new Author("Foo", "Bar"));
        book.setSection(new Section("Section"));
    }

    @WithMockUser
    @Test
    public void shouldAddBookToCard() throws Exception {
        book.setId(1L);
        book.setQuantity(1L);
        CartDto returnedCart = new CartDto();
        returnedCart.setUserEmail("user");
        returnedCart.getBooks().put(book, 1);
        when(bookService.findById(1L)).thenReturn(Optional.of(book));

        mockMvc.perform(post("/cart/")
                .queryParam("id", "1")
                .requestAttr("cart", cartDto))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model()
                        .attributeDoesNotExist("message"));
    }

    @WithMockUser
    @Test
    public void shouldDeleteFromCard() throws Exception {
        book.setId(13L);
        cartDto.setUserEmail("user");
        cartDto.getBooks().put(book, 2);

        mockMvc.perform(get("/cart/")
                .queryParam("delete", "13")
                .requestAttr("cart", cartDto))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model()
                        .attribute("cart",
                                new CartDto("user", new HashMap<>())));
    }
}
