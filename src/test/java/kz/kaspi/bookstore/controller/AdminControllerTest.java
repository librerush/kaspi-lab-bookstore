package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.model.Author;
import kz.kaspi.bookstore.model.Section;
import kz.kaspi.bookstore.service.AuthorService;
import kz.kaspi.bookstore.service.BookService;
import kz.kaspi.bookstore.service.SectionService;
import kz.kaspi.bookstore.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(AdminController.class)
public class AdminControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private BookService bookService;

    @MockBean
    private SectionService sectionService;

    @MockBean
    private AuthorService authorService;

    private final Section section = new Section("Computer Science");

    private final Author author = new Author("Jay", "Sussman");

    @WithMockUser(authorities = "USER")
    @Test
    public void shouldThrowForbiddenExceptionForUser() throws Exception {
        mockMvc.perform(get("/admin/"))
                .andExpect(status().isForbidden());
    }

    @WithMockUser(authorities = "ADMIN")
    @Test
    public void shouldShowIndex() throws Exception {
        mockMvc.perform(get("/admin/"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("admin"));
    }

    @WithMockUser(authorities = "ADMIN")
    @Test
    public void shouldShowSectionList() throws Exception {
        List<Section> sectionList = new ArrayList<>();
        sectionList.add(section);
        when(sectionService.getAll()).thenReturn(sectionList);

        mockMvc.perform(get("/admin/section/list"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("sections", sectionList));
    }

    @WithMockUser(authorities = "ADMIN")
    @Test
    public void shouldAddAuthor() throws Exception {
        mockMvc.perform(post("/admin/author/")
                .param("first_name", "Foo")
                .param("last_name", "Bar"))
                .andExpect(status().isOk());
    }

    @WithMockUser(authorities = "ADMIN")
    @Test
    public void shouldShowBooks() throws Exception {
        List<Section> sectionList = Collections.singletonList(section);
        List<String> authorList = Collections
                .singletonList(String.format("%s %s", author.getFirstName(), author.getLastName()));

        when(sectionService.getAll()).thenReturn(sectionList);
        when(authorService.getAllFullNames()).thenReturn(authorList);

        mockMvc.perform(get("/admin/book"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("sections", sectionList))
                .andExpect(model().attribute("authors", authorList));
    }
}
