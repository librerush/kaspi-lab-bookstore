package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.dto.LoginDto;
import kz.kaspi.bookstore.model.User;
import kz.kaspi.bookstore.service.BookService;
import kz.kaspi.bookstore.service.UserDetailsServiceImpl;
import kz.kaspi.bookstore.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(LoginController.class)
public class LoginControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private UserService userService;

    @MockBean
    private BookService bookService;

    @Test
    public void shouldShowIndex() throws Exception {
        when(bookService.getAll()).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("books", Collections.emptyList()));
    }

    @Test
    public void shouldShowErrorMsg() throws Exception {
        mockMvc.perform(get("/user/login?error=true"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"))
                .andExpect(model().attributeExists("loginDto"))
                .andExpect(model().attribute("error", "Wrong email or password"));
    }

    @Test
    public void shouldLoginForExistingUser() throws Exception {
        LoginDto loginDto = new LoginDto();
        loginDto.setEmail("user@mail.com");
        loginDto.setPassword("pass");

        User user = mock(User.class);
        UserDetails userDetails = mock(UserDetails.class);
        when(userService.userExist(loginDto)).thenReturn(true);
        when(userService.findUserByEmail("user@mail.com"))
                .thenReturn(Optional.of(user));
        when(userDetailsService.loadUserByUsername("user@mail.com"))
                .thenReturn(userDetails);

        mockMvc.perform(post("/user/login/")
                .param("email", loginDto.getEmail())
                .param("password", loginDto.getPassword()))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("user", userDetails));
    }
}
