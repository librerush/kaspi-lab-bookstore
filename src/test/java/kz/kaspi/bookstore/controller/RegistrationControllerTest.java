package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.dto.UserDto;
import kz.kaspi.bookstore.model.User;
import kz.kaspi.bookstore.service.UserDetailsServiceImpl;
import kz.kaspi.bookstore.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(RegistrationController.class)
public class RegistrationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private UserService userService;

    @Mock
    private UserDetails userDetails;

    private UserDto userDto;

    private User user;

    @BeforeEach
    public void setUp() {
        userDto = new UserDto();
        userDto.setEmail("user@mail.com");
        userDto.setName("user");
        userDto.setPassword("pass");
        userDto.setMatchingPassword("pass");

        user = new User();
        user.setName("user");
        user.setEmail("user@mail.com");
    }

    @Test
    public void shouldRegisterNewUser() throws Exception {
        when(userService.registerNewUserAccount(userDto)).thenReturn(user);
        when(userDetailsService.UserToUserDetails(user)).thenReturn(userDetails);

        buildPostRequest(userDto)
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void shouldShowEmailAlreadyExistsErrorMsg() throws Exception {
        when(userService.registerNewUserAccount(userDto))
                .thenThrow(new IllegalArgumentException());

        buildPostRequest(userDto)
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model()
                        .attribute("message", "An account for that email already exists."));
    }

    @Test
    public void shouldShowPasswordsDontMatchErrorMsg() throws Exception {
        userDto.setPassword("wrong");
        buildPostRequest(userDto)
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model()
                        .attribute("message", "Passwords must be same"));
    }

    private ResultActions buildPostRequest(UserDto userDto) throws Exception {
        return mockMvc.perform(post("/user/registration/")
                .param("name", userDto.getName())
                .param("email", userDto.getEmail())
                .param("password", userDto.getPassword())
                .param("matchingPassword", userDto.getMatchingPassword()));
    }
}
