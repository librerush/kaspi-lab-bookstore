package kz.kaspi.bookstore.controller;

import kz.kaspi.bookstore.model.Author;
import kz.kaspi.bookstore.model.Book;
import kz.kaspi.bookstore.service.BookService;
import kz.kaspi.bookstore.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
@WebMvcTest(BookController.class)
public class BookControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private BookService bookService;

    private Book book;

    private Author author;

    @BeforeEach
    public void setUp() {
        author = new Author("Foo", "Bar");
        book = new Book();
        book.setName("Name");
        book.setAuthor(author);
    }

    @Test
    public void shouldFindBookById() throws Exception {
        when(bookService.findById(any())).thenReturn(Optional.of(book));

        mockMvc.perform(get("/book/{id}", 123L)
                .contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("book", book));
    }

    @Test
    public void shouldFindBooksByName() throws Exception {
        List<Book> list = Collections.singletonList(book);
        when(bookService.findByName("Name")).thenReturn(list);

        mockMvc.perform(get("/book/?q={name}", "Name")
                .contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("books", list));
    }
}
