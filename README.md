# Book store

![Build Status](https://gitlab.com/librerush/kaspi-lab-bookstore/badges/master/pipeline.svg)

Environment variables:
- ADMIN_PASSWORD
- DB_USER
- DB_PASSWORD

Admin user:
- Email: admin@bookstore.kz
- Name: admin
- Password: ${ADMIN_PASSWORD}

## How to build
Create **.env** file with content:
```
ADMIN_PASSWORD=...
DB_USER=...
DB_PASSWORD=...
```
And run:
```shell
docker-compose up -d
```

In addition to project I have implemented:
- Unit and Integration tests
- Full-Text Search (using PostgreSQL's ts_vector)
